const mongoose = require('mongoose');

const recipeSchema = mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Please provide a title for the recipe'],
  },
  description: {
    type: String,
    required: [true, 'Please provide a description for the recipe'],
  },
  ingredients: {
    type: [String],
    required: [true, 'Please list the ingredients'],
  },
  instructions: {
    type: [String],
    required: [true, 'Please provide cooking instructions'],
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User', // Reference to the User model for the author of the recipe
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Recipe", recipeSchema);
