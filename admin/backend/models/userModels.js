const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const userSchema = mongoose.Schema({
 
  name: {
    type: String,
    required: [true, 'Please tell us your name!']
  },
  email: {
    type: String,
    required: [true, 'Please tell us your email!'],
    unique: true,
    lowercase:true,
    validate:[validator.isEmail, 'Please provide a valid email']
  },
  password: {
    type:String,
    required:[true, 'Please provide a password!'],
    minlength:8,
    select:false,
  },
  passwordConfirm: {
    type: String,
    required: [true, 'Please confrim your password'],
    validate: {
      validator: function(el){
        return el === this.password
      },
      message: 'Passwords are not the same',
    }
  },

});
userSchema.pre('save', async function (next){
  if (!this.isModified('password')) return next ()

  this.password = await bcrypt.hash(this.password,12)

  this.passwordConfirm = undefined
})
userSchema.pre('findOneAndUpdate', async function (next){
  const update = this.getUpdate();
  if (update.password !== '' && 
      update.password !== undefined &&
      update.password !== update.passwordConfirm){
        this.getUpdate().password = await bcrypt.hash(update.password, 12)


        update.passwordConfirm = undefined
        next()
      }else
      next()
})
userSchema.methods.correctPassword = async function (
  candidatePassword,
  userPassword,
){
  return await bcrypt.compare(candidatePassword, userPassword)
}
module.exports = mongoose.model("User", userSchema);
