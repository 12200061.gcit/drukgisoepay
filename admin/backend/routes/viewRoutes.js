const express = require('express')
const router = express.Router()
const viewController = require('./../controllers/viewController')


router.get('/', viewController.getHome)
router.get('/login', viewController.getLoginForm)
router.get('/signup', viewController.getSignupForm)
router.get('/admin', viewController.getAdminLoginForm)
router.get('/recipe', viewController.getRecipeForm)
module.exports = router
