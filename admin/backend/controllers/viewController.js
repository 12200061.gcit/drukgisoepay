const path = require('path')

exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../', 'views', 'LoginPage.html'))
}

exports.getSignupForm = (req,res) => {
    res.sendFile(path.join(__dirname,'../', 'views', 'Register.html'))
}

exports.getHome = (req,res) => {
    res.sendFile(path.join(__dirname,'../', 'views', 'Homepage.html'))
}
exports.getAdminLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../', 'views', 'AdminLoginPage.html'))
}
exports.getRecipeForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../', 'views', 'CreateRecipe.html'))
}