const Recipe = require('../models/recipeModels');

// Get all recipes
exports.getAllRecipes = async (req, res, next) => {
  try {
    const recipes = await Recipe.find();
    res.status(200).json({ data: recipes, status: 'success' });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// Create a new recipe
exports.createRecipe = async (req, res) => {
  try {
    const recipe = await Recipe.create(req.body);
    res.json({ data: recipe, status: 'success' });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// Get a specific recipe by ID
exports.getRecipe = async (req, res) => {
  try {
    const recipe = await Recipe.findById(req.params.id);
    res.json({ data: recipe, status: 'success' });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// Update a recipe by ID
exports.updateRecipe = async (req, res) => {
  try {
    const recipe = await Recipe.findByIdAndUpdate(req.params.id, req.body, { new: true });
    res.json({ data: recipe, status: 'success' });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

// Delete a recipe by ID
exports.deleteRecipe = async (req, res) => {
  try {
    const recipe = await Recipe.findByIdAndDelete(req.params.id);
    res.json({ data: recipe, status: 'success' });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};
