import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './navbar.css';
import Logo from '../../Asssets/3.png';

const SideNavBar = () => {
  const [activeItem, setActiveItem] = useState('/admin/home');

  const handleItemClick = (path) => {
    setActiveItem(path);
  };

  return (
    <div className='position-sticky sticky-top top-0' style={{justifyContent:'center',display:'flex'}}>
      <ul className='navBar position-fixed' style={{marginTop:'50px',width:'300px',paddingRight:'30px'}}>
      <li className='nav-items' style={{marginRight:'30px'}}>
          <Link to='/admin/home'>
            <img src={Logo} alt='logo' width={210} height={80}  />
          </Link>
        </li>
        <hr style={{ border: '1px solid white' }} />
        <li className={`nav-items ${activeItem === '/admin/home' ? 'active' : ''}`} style={{backgroundColor:'white',padding:'14px 10px 7px 20px',borderRadius:'5px'}}>
          <Link to='/admin/home' onClick={() => handleItemClick('/admin/home')}>
            <i className='fa-solid fa-house pe-2' style={{ color: '#4cbb17',marginRight:'40px'}}></i> Home
          </Link>
        </li>
        <li className={`nav-items ${activeItem === '/admin/feedback' ? 'active' : ''}`} style={{backgroundColor:'white',padding:'14px 10px 7px 20px',borderRadius:'5px'}}>
          <Link to='/admin/feedback' onClick={() => handleItemClick('/admin/feedback')}>
            <i className='fa-solid fa-comment pe-2' style={{ color: '#4cbb17',marginRight:'40px' }}></i>Feedback
          </Link>
        </li>
        
      
        <li className={`nav-items ${activeItem === '/admin/recipe' ? 'active' : ''}`} style={{backgroundColor:'white',padding:'14px 10px 7px 20px',borderRadius:'5px'}}>
          <Link to='/admin/recipe' onClick={() => handleItemClick('/admin/recipe')}>
            <i className='fa-solid fa-bag-shopping pe-2' style={{ color: '#4cbb17',marginRight:'40px' }}></i>Recipe List
          </Link>
        </li>
      
       
        <li className='nav-items mt-5 pe-4'>
          <Link to='/' id='logoutBtn' style={{ width: '100%', border: '2px solid white', textAlign: 'center', padding: '6px',color:'white' }}>
            Log Out
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default SideNavBar;
