import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Modal, Form, Button } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import MainWrapperContainer from '../Home/MainWrapperContainer';

const Recipe = () => {
const [showModal, setShowModal] = useState(false);
const handleModalToggle = () => {
setShowModal(!showModal);
};
const [items, setItems] = useState([]);
const [modalOpen, setModalOpen] = useState(false);
const [editItemId, setEditItemId] = useState(null); 
const [formData, setFormData] = useState({
recipe_name: '',
category: '',
preptime: '',
cookingtime: '',
calories: '',
ingredients: '',
steps: '',
description: '',
item_image: null,
});

useEffect(() => {
fetchItems();
}, []);

const fetchItems = async () => {
try {
const response = await axios.get('https://drukgisoepay.onrender.com/iteminfo');
setItems(response.data);
} catch (error) {
console.error('Error fetching items:', error);
toast.error('Failed to fetch items');
}
};

const handleInputChange = (e) => {
  const { name, value } = e.target;

  if (name === 'category') {
    setFormData((prevState) => ({
      ...prevState,
      category: value,
    }));
  } else {
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  }
};

const handleFileChange = (e) => {
const file = e.target.files[0];
setFormData((prevState) => ({
...prevState,
item_image: file,
}));
};

const handleSubmit = async (e) => {
e.preventDefault();

try {
  const formDataToSend = new FormData();
  formDataToSend.append('recipe_name', formData.recipe_name);
  formDataToSend.append('category', formData.category);
  formDataToSend.append('preptime', formData.preptime);
  formDataToSend.append('cookingtime', formData.cookingtime);
  formDataToSend.append('calories', formData.calories);
  formDataToSend.append('ingredients', formData.ingredients);
  formDataToSend.append('steps', formData.steps);
  formDataToSend.append('description', formData.description);
  formDataToSend.append('item_image', formData.item_image);

  if (editItemId) {
    // If editing an existing item, send a PUT request instead of POST
    await axios.put(`https://drukgisoepay.onrender.com/iteminfo/${editItemId}`, formDataToSend);
    toast.success('Item updated successfully');
  } else {
    // If creating a new item, send a POST request
    await axios.post('https://drukgisoepay.onrender.com/iteminfo/', formDataToSend);
    toast.success('Item created successfully');
  }

  setModalOpen(false);
  setEditItemId(null); // Reset editItemId after editing
  setFormData({
    recipe_name: '',
    category: '',
    preptime: '',
    cookingtime: '',
    calories: '',
    ingredients: '',
    steps: '',
    description: '',
    item_image: null,
  });
  fetchItems();
} catch (error) {
  console.error('Error creating/updating item:', error);
  setModalOpen(true);
  toast.error('Failed to create/update item');
}
};

const handleEdit = (item) => {
setEditItemId(item.items_id);
setFormData({
recipe_name: item.recipe_name,
category: item.category,
preptime: item.preptime,
cookingtime: item.cookingtime,
calories: item.calories,
ingredients: item.ingredients,
steps: item.steps,
description: item.description,
item_image: null,
});
setModalOpen(true);
};

const handleDelete = async (items_id) => {
try {
  console.log(items_id,"hello0")
  await axios.delete(`https://drukgisoepay.onrender.com/iteminfo/${items_id}`);
  setShowModal(false);

  toast.success('Item deleted successfully');
  fetchItems();
} catch (error) {
  console.error('Error deleting item:', error);
  toast.error('Failed to delete item');
}
};

return (
<div>
<MainWrapperContainer>
<div>

<div className='page-header'>
<div className='row' style={{marginBottom:'30px'}}>

<div className='col-sm-6' style={{marginTop:"40px"}}>
<button className='btn btn-primary float-end' style={{backgroundColor:'#4cbb17',border:'none',marginBottom:'20px',padding:'10px 20px',marginTop:'20px'}} onClick={() => setModalOpen(true)}>
Add New Recipe
</button>
</div>
</div>
</div>
<div className='row'>
<div className='col-sm-12' style={{padding:"50px"}}>

<table className='table table-hover table-center'>
<thead>
<tr>
<th>Recipe Image</th>
<th>Recipe Name</th>
<th>Category</th>
<th>Preparation Time</th>
<th>Cooking Time</th>
<th>Calories</th>
<th>Actions</th>
</tr>
</thead>
<tbody>
{items.map((item) => (
<tr key={item.items_id} style={{height:'100px',paddingTop:'auto'}}>
  <td>
                      {item.item_image_url && (
                          <img
                            className='p-0 m-0'
                            src={`https://drukgisoepay.onrender.com/uploads/${encodeURIComponent(
                              item.item_image_url
                            )}`}
                            style={{ width: '80%', height: '100px', objectFit: 'cover', objectPosition: 'center' }}
                            alt="item"
                          />
                        )}
                        </td>
                        <td>{item.recipe_name}</td>
                        <td>{item.category}</td>
                        <td>{item.preptime}</td>
                        <td>{item.cookingtime}</td>
                        <td>{item.calories}</td>
                        <td >
<button style={{marginRight:"40px",border:'none',background:'transparent',fontSize:'16px',fontWeight:'bold',color:'green'}} onClick={() => handleEdit(item)} >
Edit
</button>

<button style={{border:'none',background:'transparent',fontSize:'16px',fontWeight:'bold',color:'red'}}  onClick={() => handleModalToggle(item.items_id)}>
Delete
</button>
<Modal show={showModal} onHide={() => setShowModal(false)}>
    <Modal.Header closeButton>
      <Modal.Title>Confirmation</Modal.Title>
    </Modal.Header>
    <Modal.Body>Are you sure you want to delete this item?</Modal.Body>
    <Modal.Footer>
      <Button variant='secondary' onClick={() => setShowModal(false)}>
        Cancel
      </Button>
      <Button variant='danger' onClick={() => handleDelete(item.items_id)}>
        Delete
      </Button>
    </Modal.Footer>
  </Modal>
</td>
</tr>
))}
</tbody>
</table>

</div>
</div>
</div>
</MainWrapperContainer>

  <Modal show={modalOpen} onHide={() => setModalOpen(false)}  >
    <Modal.Header closeButton >
      <Modal.Title>{editItemId ? 'Edit Recipe' : 'Add New Recipe'}</Modal.Title>
    </Modal.Header>
    <Modal.Body >
      <Form onSubmit={handleSubmit} >
      <div style={{display:'flex',justifyContent:'space-between'}}>
        <Form.Group className='mb-3'>
          <Form.Label>Recipe Name</Form.Label>
          <Form.Control
            type='text'
            name='recipe_name'
            value={formData.recipe_name}
            onChange={handleInputChange}
            required
          />
        </Form.Group>

        <Form.Group className='mb-3'>
        <Form.Label>Category</Form.Label>
            <Form.Control
              as='select'
              name='category'
              value={formData.category}
              onChange={handleInputChange}
              required
            >
              <option value=''>Select a category</option>
              <option value='Cuisines'>Cuisines</option>
              <option value='Bake'>Baked Items</option>
              <option value='Vegetables'>Vegetables</option>
              <option value='Desserts'>Desserts</option>
              <option value='Beverages'>Beverages</option>
            </Form.Control>
        </Form.Group>


        </div>
        <div style={{display:'flex',justifyContent:'space-between'}}>

        <Form.Group className='mb-3'>
          <Form.Label>Preparation Time</Form.Label>
          <Form.Control
            type='text'
            name='preptime'
            value={formData.preptime}
            onChange={handleInputChange}
            required
          />
        </Form.Group>
        <Form.Group className='mb-3'>
          <Form.Label>Cooking Time</Form.Label>
          <Form.Control
            type='text'
            name='cookingtime'
            value={formData.cookingtime}
            onChange={handleInputChange}
            required
          />
        </Form.Group>
        </div>

        <Form.Group className='mb-3'>
          <Form.Label>Calories</Form.Label>
          <Form.Control
            type='text'
            name='calories'
            value={formData.calories}
            onChange={handleInputChange}
            required
          />
        </Form.Group>
        <Form.Group className='mb-3'>
          <Form.Label>Ingredients</Form.Label>
          <Form.Control
            as='textarea'
            rows={3}
            name='ingredients'
            value={formData.ingredients}
            onChange={handleInputChange}
            required
          />
        </Form.Group>
        <Form.Group className='mb-3'>
          <Form.Label>Steps</Form.Label>
          <Form.Control
            as='textarea'
            rows={3}
            name='steps'
            value={formData.steps}
            onChange={handleInputChange}
            required
          />
        </Form.Group>
        <Form.Group className='mb-3'>
          <Form.Label>Description</Form.Label>
          <Form.Control
            as='textarea'
            rows={3}
            name='description'
            value={formData.description}
            onChange={handleInputChange}
            required
          />
        </Form.Group>
        <Form.Group className='mb-3'>
          <Form.Label>Image</Form.Label>
          <Form.Control type='file' name='item_image' onChange={handleFileChange} />
        </Form.Group>
        <Button variant='primary' type='submit' style={{backgroundColor:'#4cbb17'}}>
          {editItemId ? 'Update Recipe' : 'Add Recipe'}
        </Button>
      </Form>
    </Modal.Body>
  </Modal>



  <ToastContainer />
</div>
);
};

export default Recipe;