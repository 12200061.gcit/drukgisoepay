import React from 'react'
import './Login.css'
import logo from '../../Asssets/greenlogo.png'

function Login() {
  return (
    <div className='LoginContainer'>
        <div className="container login-box" style={{width:'500px'}}>
            <div className="row">
                <div className="col-12">
             
                    <div className='text-center'>
                        <img src={logo} alt="logo" width={400} height={130} style={{marginBottom:'20px'}} />
                    </div>
                    <form action="/admin/home">
                        <div className='input__Container mt-4'>
                            <input type="text" name="" id=""placeholder='User Name' required />
                            <br/>
                            <br/>

                            <input type="password" name="" id="" placeholder='Password' required />
                        </div>
                        <div className='text-center'>
                            <button className='login_button' style={{width:'100%',marginTop:'60px'}}>Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  )
}
export default Login;