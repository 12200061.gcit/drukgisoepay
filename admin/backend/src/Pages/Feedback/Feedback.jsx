import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../Home/MainWrapperContainer';

function Homepage() {
  const [items, setItems] = useState([]);

  const handleDelete = async (itemId) => {
    try {
      console.log(itemId)

      await axios.delete(`https://drukgisoepay.onrender.com/getfeedback/${itemId}`);
      toast.success('Item deleted successfully');
      fetchItems();
    } catch (error) {
      console.error('Error deleting item:', error);
      toast.error('Failed to delete item');
    }
  };

  useEffect(() => {
    fetchItems();
  }, []);

  const fetchItems = async () => {
    try {
      const response = await axios.get('https://drukgisoepay.onrender.com/getfeedback');
      setItems(response.data);
    } catch (error) {
      console.error('Error fetching items:', error);
      toast.error('Failed to fetch items');
    }
  };

  return (
    <>
      <MainWrapperContainer>
     

        <table
          className="table tabletable-center"
          style={{ margin: '10%', width: '70%' }}
        >
          <thead>
            <tr>
              <th>username</th>
              <th>email</th>
              <th>messages</th>
            </tr>
          </thead>
          <tbody>
            {items.map((item) => (
              <tr key={item.id} style={{ height: '100px', paddingTop: 'auto' }}>
                <td>{item.username}</td>
                <td>{item.email}</td>
                <td>{item.message}</td>
                <td></td>
                <td>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </MainWrapperContainer>
    </>
  );
}

export default Homepage;
