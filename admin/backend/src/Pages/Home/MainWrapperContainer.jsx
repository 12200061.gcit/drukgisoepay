import React from 'react'
import SideNavBar from '../../Component/SideNavBar/SideNavBar'
function MainWrapperContainer ({children}){
  return (
    <div className='container-fluid'>
        <div className="row vh-100 position-sticky sticky-top top-0">
            <div className="col-2" style={{backgroundColor:'#4cbb17'}}>
                <SideNavBar/>
            </div>
            <div className="col-10 m-0 p-0">
                {children}
            </div>
        </div>
      
    </div>
  )
}

export default MainWrapperContainer;