import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Modal, Form, Button } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../Home/MainWrapperContainer';
import './home.css';

function Homepage() {
  const [items, setItems] = useState([]);
  const [feedbackcount, setFeedbackcount] = useState(null);
  const [usercount, setUsercount] = useState(null);
  const [recipecount, setRecipecount] = useState(null);
  const [showModal, setShowModal] = useState(false);

  const handleModalToggle = () => {
    setShowModal(!showModal);
  };

  const handleDelete = async (items_id) => {
    try {
      console.log(items_id, "hello0");
      await axios.delete(`https://drukgisoepay.onrender.com/iteminfo/${items_id}`);
      setShowModal(false);
      toast.success('Item deleted successfully');
      fetchItems();
    } catch (error) {
      console.error('Error deleting item:', error);
      toast.error('Failed to delete item');
    }
  };

  useEffect(() => {
    fetchItems();
    fetchrecipecounts();
    fetchfeedbackcounts();
    fetchusercounts();
  }, []);

  const fetchItems = async () => {
    try {
      const response = await axios.get('https://drukgisoepay.onrender.com/getuser');
      setItems(response.data);
    } catch (error) {
      console.error('Error fetching items:', error);
      toast.error('Failed to fetch items');
    }
  };

  const fetchrecipecounts = async () => {
    try {
      const response = await axios.get('https://drukgisoepay.onrender.com/recipes');
      const { recipecount } = response.data;
      setRecipecount(recipecount);
    } catch (error) {
      console.error('Error fetching recipe count:', error);
    }
  };

  const fetchfeedbackcounts = async () => {
    try {
      const response = await axios.get('https://drukgisoepay.onrender.com/feedbacks');
      const { feedbackcount } = response.data;
      setFeedbackcount(feedbackcount);
    } catch (error) {
      console.error('Error fetching feedback count:', error);
    }
  };

  const fetchusercounts = async () => {
    try {
      const response = await axios.get('https://drukgisoepay.onrender.com/userss');
      const { usercount } = response.data;
      setUsercount(usercount);
    } catch (error) {
      console.error('Error fetching user count:', error);
    }
  };

  return (
    <>
      <MainWrapperContainer>
        <div className="row">
          <div className="col-12">
          </div>
          <div className="info-row">
            <div className="info-box">
              <p>Total User</p>
              <h3>{usercount}</h3>
            </div>
            <div className="info-box">
              <p>Total Feedback</p>
              <h3>{feedbackcount}</h3>
            </div>
            <div className="info-box">
              <p>Total Recipe</p>
              <h3>{recipecount}</h3>
            </div>
          </div>
        </div>

        <table className="table table-hover table-center" style={{margin:"40px",width:'40%',marginLeft:'30%'}}>
          <thead>
            <tr>
              <th>Username</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {items.map((item) => (
              <tr key={item.id} style={{ height: '100px', paddingTop: 'auto' }}>
                <td>{item.username}</td>
                <td>{item.email}</td>
                <td>                 
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </MainWrapperContainer>
    </>
  );
}

export default Homepage;
