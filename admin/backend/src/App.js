import React from "react";
import Recipe from "./Pages/Recipe/recipe";
import Login from "./Pages/Login/Login"
import Feedback from "./Pages/Feedback/Feedback";
import Homepage from "./Pages/Home/homepage";
import { BrowserRouter,Routes,Route } from "react-router-dom";
function App() {
  return (
    <div>
        <BrowserRouter>
          <Routes>
            <Route exact path="/" Component={Login}/>
            <Route path="/admin/home" Component={Homepage}/>
            <Route path="/admin/feedback" Component={Feedback}/>
            <Route path="/admin/recipe" Component={Recipe}/>
      
          </Routes>
        </BrowserRouter>
    </div>
  
  );
}

export default App;
