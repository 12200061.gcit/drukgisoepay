// Select the registration form element
const registerForm = document.getElementById('register-form');

// Add an event listener for form submission
registerForm.addEventListener('submit', function (event) {
    event.preventDefault(); // Prevent the default form submission

    // Get the registration input values
    const email = document.getElementById('email').value;
    const newPassword = document.getElementById('new-password').value;
    const confirmPassword = document.getElementById('confirm-password').value;
    const name = document.getElementById('name').value; // Add this line to get the user's name

    // Check if the passwords match
    if (newPassword !== confirmPassword) {
        alert('Passwords do not match. Please try again.');
        return;
    }

    // Create a JSON object with the registration data
    const registrationData = {
        name: name, // Add the user's name
        email: email,
        password: newPassword,
        passwordConfirm: confirmPassword
    };

    // Make an AJAX request to the server for registration
    fetch('/api/v1/users/signup', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(registrationData),
    })
    .then((response) => {
        if (!response.ok) {
            throw new Error('Registration failed'); // Handle registration error
        }
        return response.json();
    })
    .then((data) => {
        // Registration successful, redirect to the login page
        alert('Registration successful. You can now login.');
        window.location.href = 'LoginPage.html'; // Replace with your actual login page URL
    })
    .catch((error) => {
        // Registration failed, show an error message or handle it accordingly
        alert('Registration failed. Please try again.');
        console.error(error);
    });
});
