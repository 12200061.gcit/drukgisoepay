// Select the recipe form element
const recipeForm = document.getElementById('recipeForm');

// Add an event listener for form submission
recipeForm.addEventListener('submit', function (event) {
    event.preventDefault(); // Prevent the default form submission

    // Get the recipe input values
    const title = document.getElementById('recipe-name').value;
    const category = document.getElementById('recipe-category').value;
    const calories = document.getElementById('calories').value;
    const prepTime = document.getElementById('prep-time').value;
    const cookTime = document.getElementById('cook-time').value;
    const ingredients = document.getElementById('ingredients').value;
    const description = document.getElementById('description').value;
    const instructions = document.getElementById('steps').value;

    // Create a JSON object with the recipe data
    const recipeData = {
        title: title,
        category: category,
        calories: calories,
        prepTime: prepTime,
        cookTime: cookTime,
        ingredients: ingredients,
        description: description,
        instructions: instructions,
    };

    // Make an AJAX request to the server for recipe submission
   fetch('/api/v1/recipes/createRecipe', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        
    },
    body: JSON.stringify(recipeData),
})
    .then((response) => {
        if (!response.ok) {
            throw new Error('Recipe submission failed'); // Handle submission error
        }
        return response.json();
    })
    .then((data) => {
        // Recipe submission successful, show a success message or redirect to a success page
        alert('Recipe submitted successfully.');
        // Redirect to a success page or perform other actions
        window.location.href = 'RecipeList.html'; // Replace with your actual success page URL
    })
    .catch((error) => {
        // Recipe submission failed, show an error message or handle it accordingly
        alert('Recipe submission failed. Please try again.');
        console.error(error);
    });
});
