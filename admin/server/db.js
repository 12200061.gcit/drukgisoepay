const Pool = require('pg').Pool

const pool = new Pool({
    user: process.env.USERNAME,
    password: process.env.PASSWORD,
    host: process.env.HOSTNAME,
    port: 5432,
    database:process.env.DATABASE_NAME
})

module.exports = pool
