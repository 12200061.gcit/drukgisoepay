const express = require('express');
const router = express.Router();
const pool = require('../db');

// Create user feedback
router.post('/feedback', async (req, res) => {
  try {
    const { username, email, subject, description } = req.body;

    // Insert the user feedback into the database
    const query = 'INSERT INTO user_feedback (username, email, subject, description) VALUES ($1, $2, $3, $4) RETURNING *';
    const values = [username, email, subject, description];

    const result = await pool.query(query, values);
    const createdFeedback = result.rows[0];

    res.status(201).json(createdFeedback);
  } catch (error) {
    console.error('Error creating user feedback:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Get all user feedbacks
router.get('/feedback', async (req, res) => {
  try {
    // Retrieve all user feedbacks from the database
    const query = 'SELECT * FROM user_feedback';

    const result = await pool.query(query);
    const feedbacks = result.rows;

    res.status(200).json(feedbacks);
  } catch (error) {
    console.error('Error retrieving user feedbacks:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Update user feedback
router.put('/feedback/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { username, email, subject, description } = req.body;

    // Check if the user feedback with the specified ID exists
    const checkQuery = 'SELECT * FROM user_feedback WHERE id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingFeedback = checkResult.rows[0];
    if (!existingFeedback) {
      return res.status(404).json({ error: 'User feedback not found' });
    }

    // Update the user feedback in the database
    const updateQuery = 'UPDATE user_feedback SET username = $1, email = $2, subject = $3, description = $4 WHERE id = $5 RETURNING *';
    const updateValues = [username, email, subject, description, id];

    const updateResult = await pool.query(updateQuery, updateValues);
    const updatedFeedback = updateResult.rows[0];

    res.status(200).json(updatedFeedback);
  } catch (error) {
    console.error('Error updating user feedback:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Delete user feedback
router.delete('/feedback/:id', async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the user feedback with the specified ID exists
    const checkQuery = 'SELECT * FROM user_feedback WHERE id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingFeedback = checkResult.rows[0];
    if (!existingFeedback) {
      return res.status(404).json({ error: 'User feedback not found' });
    }

    // Delete the user feedback from the database
    const deleteQuery = 'DELETE FROM user_feedback WHERE id = $1';
    await pool.query(deleteQuery, [id]);

    res.status(200).json({ message: 'User feedback deleted successfully' });
  } catch (error) {
    console.error('Error deleting user feedback:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;
