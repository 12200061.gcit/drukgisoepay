const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const pool = require('../db');

// File upload configuration
const storage = multer.diskStorage({
  destination: './uploads/',
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const fileExtension = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + uniqueSuffix + fileExtension);
  },
});

const upload = multer({ storage });

// Create a recipe
router.post('/', upload.single('item_image'), async (req, res) => {
  try {
    const {recipe_name, category, preptime, cookingtime, calories, ingredients, steps, description } = req.body;
    const item_image = req.file;

    // Insert the recipe into the database
    const query = 'INSERT INTO recipe (item_image_url, recipe_name, category, preptime, cookingtime, calories, ingredients, steps, description) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *';
    const values = [item_image.filename, recipe_name, category, preptime, cookingtime, calories, ingredients, steps, description];

    const result = await pool.query(query, values);
    const createdRecipe = result.rows[0];

    res.status(201).json(createdRecipe);
  } catch (error) {
    console.error('Error creating recipe:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Get all recipes
router.get('/', async (req, res) => {
  try {
    // Retrieve all recipes from the database
    const query = 'SELECT * FROM recipe';

    const result = await pool.query(query);
    const recipes = result.rows;

    res.status(200).json(recipes);
  } catch (error) {
    console.error('Error retrieving recipes:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Update a recipe
router.put('/:id', upload.single('item_image'), async (req, res) => {
  try {
    const { id } = req.params;
    const { item_image_url, recipe_name, category, preptime, cookingtime, calories, ingredients, steps, description } = req.body;
    const item_image = req.file;

    // Check if the recipe with the specified ID exists
    const checkQuery = 'SELECT * FROM Recipe WHERE items_id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingRecipe = checkResult.rows[0];
    if (!existingRecipe) {
      return res.status(404).json({ error: 'Recipe not found' });
    }

    // Update the recipe in the database
    let itemImageFilename = existingRecipe.item_image_url; // Keep the current item image filename by default

    if (item_image) {
      // If a new item image is provided, update the item image filename
      itemImageFilename = item_image.filename;
    }

    const updateQuery = 'UPDATE recipe SET item_image_url = $1, recipe_name = $2, category = $3, preptime = $4, cookingtime = $5, calories = $6, ingredients = $7, steps = $8, description = $9 WHERE items_id = $10 RETURNING *';
    const updateValues = [itemImageFilename, recipe_name, category, preptime, cookingtime, calories, ingredients, steps, description, id];

    const updateResult = await pool.query(updateQuery, updateValues);
    const updatedRecipe = updateResult.rows[0];

    res.status(200).json(updatedRecipe);
  } catch (error) {
    console.error('Error updating recipe:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Delete an item
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the item with the specified ID exists
    const checkQuery = 'SELECT * FROM recipe WHERE items_id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingItem = checkResult.rows[0];
    if (!existingItem) {
      return res.status(404).json({ error: 'Item not found' });
    }

    // Delete the item from the database
    const deleteQuery = 'DELETE FROM recipe WHERE items_id = $1';
    await pool.query(deleteQuery, [id]);

    res.status(200).json({ message: 'Item deleted successfully' });
  } catch (error) {
    console.error('Error deleting item:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;
