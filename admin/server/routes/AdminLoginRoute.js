const express = require('express');
const router = express.Router();

router.post('/login', async (req, res) => {
  const { email, password } = req.body;

  try {
    // Query the database to validate admin credentials
    const query = 'SELECT * FROM admins WHERE email = $1 AND password = $2';
    const values = [email, password];
    const result = await pool.query(query, values);

    if (result.rowCount === 1) {
      // Authentication successful
      res.status(200).json({ message: 'Login successful' });
    } else {
      // Authentication failed
      res.status(401).json({ message: 'Invalid credentials' });
    }
  } catch (error) {
    // Error occurred during database query
    console.error('Error executing query:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

module.exports = router;
