const express = require('express');
const cors = require('cors');
const bcrypt = require('bcrypt')
const app = express();
const PORT = 8000;
const pool = require('./db')
const session = require('express-session');


// Import routes
const ItemInfoRouter = require('./routes/ItemInfo');
const authRouter = require('./routes/UserRegistrationRoutes');
const adminLogin = require('./routes/AdminLoginRoute');

// Middleware
app.use(cors());
app.use(express.json());

// Use routes
app.use('/profile',express.static('profile'));
app.use('/uploads', express.static('uploads')); // Serve uploaded images
app.use('/iteminfo',ItemInfoRouter);
app.use('/auth', authRouter);
app.use('/admin', adminLogin);

app.use(session({
  secret: 'your-secret-key',
  resave:false,
  saveUninitialized:true
}));

app.post('/login', async (req, res) => {
  const { email, password } = req.body;
  // Check if user with given email exists in the database
  const result = await pool.query('SELECT * FROM users WHERE email = $1', [email]);
  const user = result.rows[0];
  if (!user) {
    return res.status(401).json({ message: 'Email has been used. Try another one!' });
  }
  // Check if the provided password matches the hashed password stored in the database
  const passwordMatches = await bcrypt.compare(password, user.encrypted_password);
  if (!passwordMatches) {
    return res.status(401).json({ message: 'Invalid email or password' });
  }
  console.log("login")
  // Set the user session
  req.session.user = {
    email: user.email,
    username: user.username,
  };
  const username = req.session.user.username;
  console.log(username)
res.status(200).json({ email:user.email ,username, id:user.id, authToken:true});
});
//API endpoint for adding new User
app.post('/signup', (req, res) => {
    const { email, username, password } = req.body; 
    // Hash the password
    bcrypt.hash(password, 10, (err, hash) => {
      if (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
      } else {
        // Store the user in the database
        pool.query(
          'INSERT INTO users (username, email, encrypted_password) VALUES ($1, $2, $3)',
          [username, email, hash],
          (err, result) => {
            if (err) {
              console.error(err);
              res.status(500).json({ message: 'Internal server error' });
            } else {
              console.log('User added');
              res.status(200).json({ message: 'User added' });
            }
          }
        );
      }
    });
  });

  app.post('/logout',(req,res)=>{
    res.json({message:'Logged out Successfully'})
  })
  app.post('/upload', async (req, res) => {
    const { username, email, message } = req.body;
    console.log(username);
    console.log(email);
    console.log(message);
  
    try {
      // Check if user with given email exists in the database
      const result = await pool.query('INSERT INTO feedback(username, email, message) VALUES ($1, $2, $3) RETURNING *', [username, email, message]);
      const user = result.rows[0];
      res.status(200).json(user);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'An error occurred while saving the feedback.' });
    }
  });
  app.get('/detail/:id',async(req,res)=>{
    const {id}=req.params
    try{
      const detail=await pool.query('SELECT * FROM recipe WHERE items_id=$1;',[id])
      res.json(detail.rows)
    }
    catch(error){
      console.log(error)
    }
  })
  app.get('/getfeedback', async (req, res) => {
    try {
      const cartItem = await pool.query('SELECT * FROM feedback;')
      res.json(cartItem.rows)
  
    } catch (error) {
      console.log(error)
    }
  })
  app.get('/getuser', async (req, res) => {
    try {
      const cartItem = await pool.query('SELECT * FROM users;')
      res.json(cartItem.rows)
  
    } catch (error) {
      console.log(error)
    }
  })
  app.get("/recipes", async (req, res) => {
    try {
      const result = await pool.query("SELECT COUNT(*) FROM recipe;");
      // const count = result.rows[0].count;
      const recipecount = parseInt(result.rows[0].count, 10);
      res.json({ recipecount });
    } catch (error) {
      console.log(error);
    }
  });
  app.get("/feedbacks", async (req, res) => {
    try {
      const result = await pool.query("SELECT COUNT(*) FROM feedback;");
      // const count = result.rows[0].count;
      const feedbackcount = parseInt(result.rows[0].count, 10);
      res.json({ feedbackcount });
    } catch (error) {
      console.log(error);
    }
  });
  app.get("/userss", async (req, res) => {
    try {
      const result = await pool.query("SELECT COUNT(*) FROM users;");
      // const count = result.rows[0].count;
      const usercount = parseInt(result.rows[0].count, 10);
      res.json({ usercount });
    } catch (error) {
      console.log(error);
    }
  });

// Delete an item
app.delete('getfeedback/:id', async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the item with the specified ID exists
    const checkQuery = 'SELECT * FROM feedback WHERE id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingItem = checkResult.rows[0];
    if (!existingItem) {
      return res.status(404).json({ error: 'Item not found' });
    }

    // Delete the item from the database
    const deleteQuery = 'DELETE FROM feedback WHERE id = $1';
    await pool.query(deleteQuery, [id]);

    res.status(200).json({ message: 'Item deleted successfully' });
  } catch (error) {
    console.error('Error deleting item:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});
  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });

