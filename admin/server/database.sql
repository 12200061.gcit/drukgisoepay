-- To CREATE database
CREATE TABLE food_recipe (
    gitems_id SERIAL PRIMARY KEY,
    item_image_url VARCHAR(255),
    recipe_name VARCHAR(500),
    category VARCHAR(200),
    preptime VARCHAR(100),
    cookingtime VARCHAR(10),
    calories VARCHAR(100),
    ingredients VARCHAR(30),
    steps VARCHAR(20), 
    description VARCHAR(100)
);
