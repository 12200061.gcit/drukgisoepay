// Select the login form element
const loginForm = document.getElementById('login-form');

// Add an event listener for form submission
loginForm.addEventListener('submit', function (event) {
    event.preventDefault(); // Prevent the default form submission

    // Get the email and password input values
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;

    // Create a JSON object with the user's credentials
    const credentials = {
        email: email,
        password: password,
    };

    // Make an AJAX request to the server for authentication
    fetch('/api/v1/users/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(credentials),
    })
    .then((response) => {
        if (!response.ok) {
            throw new Error('Authentication failed'); // Handle authentication error
        }
        return response.json();
    })
    .then((data) => {
        // Authentication successful, obtain the token from the response
        const token = data.token;
        const userEmail = data.email;
        // Store the token in a secure location (e.g., browser's localStorage or sessionStorage)
        // for future use in making authenticated requests to protected routes.

        // Example of storing the token in localStorage:
        localStorage.setItem('authToken', token);
        localStorage.setItem('userEmail', userEmail);

        // Redirect to the dashboard or perform other actions
        window.location.href = 'Userhomepage.html'; // Replace with your actual dashboard URL
    })
    .catch((error) => {
        // Authentication failed, show an error message or handle it accordingly
        alert('Authentication failed. Please check your credentials.');
        console.error(error);
    });
});
