import React from "react";
import logo from "../assets/images/greenlogo.png";
import { useState } from "react";
import SigninValidation from "../pages/SigninValidation";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import { BrowserRouter as Router, Routes, Link, Route, useNavigate } from "react-router-dom";

const SignIn = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [errors, setErrors] = useState({});

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Perform client-side validation
    const validationErrors = SigninValidation({ email, password });
    if (Object.keys(validationErrors).length > 0) {
      // If there are validation errors, set the errors state and return
      setErrors(validationErrors);
      return;
    }

    try {
      const response = await axios.post("https://drukgisoepay.onrender.com/login", {
        email,
        password,
      });
      console.log(response.data); // The response will contain a message or a token for the authenticated user

      // Save the session cookie in the browser
      // Cookies.set('sessionID', response.data.sessionID, { expires: 1 });

      // after successful login
      const authToken = response.data.authToken;
      setSuccessMessage("You have successfully logged in.");

      localStorage.setItem("useremail", response.data.email);
      localStorage.setItem("id", response.data.id);
      console.log(response.data.id);
      // localStorage.setItem('username', response.data.username);

      // Cookies.set('authToken', authToken);

      // set the authentication token in local storage when the user logs in
      localStorage.setItem("authToken", authToken);
      console.log(authToken);

      navigate("/Home");

      // Set isLoggedIn to true
      //   setIsLoggedIn(true);
      //   setShowLoginForm(false);

      setSuccessMessage("You have successfully logged in.");
      setErrorMessage("");
    } catch (error) {
      console.error(error);

      setErrorMessage("Invalid email or password.");
      setSuccessMessage("");
    }
  };

  return (
    <div
      className="login"
      style={{
        width: "30%",
        margin: "auto",
        height: "800px",
        marginTop: "3%",
        padding: "1% 3%",
      }}
    >
      <div className="form_container">
        <form onSubmit={handleSubmit}>
          <div style={{marginTop:200}}>         
            <div className="text-center">
              <img
                className="img-fluid"
                width={300}
                height={300}
                src={logo}
                alt="Logo"
              />
            </div>
            <div style={{ margin: "50px 0px" }}>
              <input
                type="email"
                id="email"
                placeholder="Email address"
                value={email}
                style={{
                  padding: "20px",
                  fontSize: "16px",
                  border: "1.5px solid #4cbb17",
                }}
                className="form-control"
                onChange={handleEmailChange}
              />
              {errors.email && (
                <p className="error-message">{errors.email}</p>
              )}
            </div>
            <div className="mb-2">
              <input
                type="password"
                id="password"
                placeholder="Password"
                value={password}
                style={{
                  padding: "20px",
                  fontSize: "16px",
                  border: "1.5px solid #4cbb17",
                }}
                className="form-control"
                onChange={handlePasswordChange}
              />
              {errors.password && (
                <p className="error-message">{errors.password}</p>
              )}
            </div>

            <div className="d-grid">
              <button
                className="btn"
                style={{
                  backgroundColor: "#4cbb17",
                  padding: "15px",
                  fontSize: "16px",
                  fontWeight: "bold",
                  margin: "20px 0px",
                  color: "white",
                }}
              >
                Sign in
              </button>
            </div>
            <p
              className="text-end"
              style={{ fontSize: "16px", color: "" }}
            >
              Don't have an account?{" "}
              <Link
                className="signupLink"
                to="/SignUp"
                style={{
                  color: "#4cbb17",
                  textDecoration: "none",
                }}
              >
                Sign Up
              </Link>
            </p>
            {successMessage && (
              <div className="alert alert-success" role="alert">
                {successMessage}
              </div>
            )}
            {errorMessage && (
              <div className="alert alert-danger" role="alert">
                {errorMessage}
              </div>
            )}
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignIn;
