import React from "react";

const SigninValidation = (values) => {
    const errors = {}

    const email_pattern = /^[^\s@]+@[^s@]+\.[^s@]{2,6}$/;
    const password_pattern = /^(?=.\d)(?=.[a-z])(?=.[A-Z])[a-zA-Z0-9]{8,}$/;

    if (values.email === ""){
        errors.email = " Please enter your email address";
    }
    else if(!email_pattern.test(values.email)) {
        errors.email = "Please enter a valid email address";
    }
    if (values.password === ""){ 
        errors.password = "Please enter a password";
    }
    
return errors; 

}
export default SigninValidation;