import React, { useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import logo from "../assets/images/greenlogo.png";
import SignUpValidation from "../pages/SigninValidation";

const SignUp = () => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirm_password, setCpassword] = useState("");
  const [successMessage, setSuccessMessage] = useState("");
  const [errors, setErrors] = useState({});

  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleCpasswordChange = (event) => {
    setCpassword(event.target.value);
  };

  const validateForm = () => {
    const validationErrors = SignUpValidation({
      username,
      email,
      password,
      confirm_password,
    });
    setErrors(validationErrors);
    return Object.keys(validationErrors).length === 0;
  };

  const createUser = async (username, email, password) => {
    try {
      const response = await axios.post("https://drukgisoepay.onrender.com/signup", {
        username,
        email,
        password,
      });
      console.log(response.data);
      window.location.reload();
      setSuccessMessage("Account created successfully!");
    } catch (error) {
      console.error(error);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Perform form validation
    if (!validateForm()) {
      return;
    }

    await createUser(username, email, password);
  };

  return (
    <div
      className="login"
      style={{
        width: "30%",
        margin: "auto",
        height: "800px",
        marginTop: "3%",
        padding: "1% 3%",
      }}
    >
      <div className="form_container">
        <form onSubmit={handleSubmit}>
          <div style={{ margin:"150px 0px"}}>
          
            <div className="text-center">
              <img
                className="img-fluid"
                width={300}
                height={300}
                src={logo}
                alt="Logo"
              />
            </div>
            <div style={{ margin: "20px 0px",marginTop:50 }}>
              <input
                type="text"
                id="username"
                placeholder="Username"
                value={username}
                style={{
                  padding: "20px",
                  fontSize: "16px",
                  border: "1.5px solid #4cbb17",
                }}
                className="form-control"
                onChange={handleUsernameChange}
              />
              {errors.username && (
                <p className="error-message">{errors.username}</p>
              )}
            </div>
            <div style={{ margin: "20px 0px" }}>
              <input
                type="email"
                id="email"
                placeholder="Email address"
                value={email}
                style={{
                  padding: "20px",
                  fontSize: "16px",
                  border: "1.5px solid #4cbb17",
                }}
                className="form-control"
                onChange={handleEmailChange}
              />
              {errors.email && (
                <p className="error-message">{errors.email}</p>
              )}
            </div>
            <div style={{ margin: "20px 0px" }}>
              <input
                type="password"
                id="password"
                placeholder="Password"
                value={password}
                style={{
                  padding: "20px",
                  fontSize: "16px",
                  border: "1.5px solid #4cbb17",
                }}
                className="form-control"
                onChange={handlePasswordChange}
              />
              {errors.password && (
                <p className="error-message">{errors.password}</p>
              )}
            </div>
            <div className="mb-2">
              <input
                type="password"
                id="confirm password"
                placeholder="confirm password"
                value={confirm_password}
                style={{
                  padding: "20px",
                  fontSize: "16px",
                  border: "1.5px solid #4cbb17",
                }}
                className="form-control"
                onChange={handleCpasswordChange}
              />
              {errors.confirm_password && (
                <p className="error-message">{errors.confirm_password}</p>
              )}
            </div>

            <div className="d-grid">
              <button
                className="btn"
                style={{
                  backgroundColor: "#4cbb17",
                  padding: "15px",
                  fontSize: "16px",
                  fontWeight: "bold",
                  margin: "30px 0px",
                  color: "white",
                }}
              >
                Register
              </button>
            </div>
            <p
              className="text-end"
              style={{ fontSize: "16px", color: "" }}
            >
              Already have an account?{" "}
              <Link
                className="signupLink"
                to="/"
                style={{
                  color: "#4cbb17",
                  textDecoration: "none",
                }}
              >
                Login
              </Link>
            </p>
            {successMessage && (
              <div className="alert alert-success" role="alert">
                {successMessage}
              </div>
            )}
            {/* Add errorMessage declaration */}
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignUp;
