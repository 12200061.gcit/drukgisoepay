import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import SearchForm from '../components/Header/SearchForm';
import Navbar from '../components/Header/Navbar';
import Footer from '../components/Footer/Footer';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import LocalFireDepartmentIcon from '@mui/icons-material/LocalFireDepartment';
import './HomePage.css';
import axios from 'axios';

const HomePage = () => {
  const [searchQuery,setSearchQuery] = useState('');
  const[filterRecipes,setFilterRecipes]=useState([]);
  const [activeCategory, setActiveCategory] = useState('');
  const [recipes, setRecipes] = useState([]);

  const handleSearch=(event)=>{
    setSearchQuery(event.target.value)
  }
  useEffect(()=>{
    if(recipes){
  const filterRecipes=recipes.filter((item)=>
  item.recipe_name.toLowerCase().includes(searchQuery.toLowerCase())
  );
  setFilterRecipes(filterRecipes);
  }
},[recipes,searchQuery])
  useEffect(() => {
    const fetchRecipes = async () => {
      try {
        const response = await axios.get('http://localhost:8000/iteminfo');
        setRecipes(response.data);
        setActiveCategory('all');
      } catch (error) {
        console.error('Error fetching recipes', error);
      }
    };

    fetchRecipes();
  }, []);

  const handleCategoryClick = (category) => {
    setActiveCategory(category);
  };

  const getRecipesByCategory = (category) => {
    return recipes.filter((iteminfo) => iteminfo.category === category).slice(0, 5);
  };

  return (
    <>
      <Navbar />
      <div className="header">
        <div className="header-content flex align-center justify-center flex-column text-center">
          <SearchForm handleSearch={handleSearch}/>
          <h1 className="text-white header-title" style={{ fontSize: '28px', margin: '40px' }}>
            What are your favorite cuisines?
          </h1>
          <p className="text  my-3 ls-1" style={{ fontSize: '18px',color:'#4cbb17',fontWeight:'500' }}>
            Personalize your experience with Drukgi Soepay.The only Bhutanese webiste to provide your need for cooking
          </p>
        </div>
      </div>

      <section className="home-address-area">
        <div className="sec-title-style1 text-center max-width">
          <div className="text">
            <div className="decor-left">
              <span></span>
            </div>
            <p style={{fontSize:'24px'}}>List of Recipes</p>
            <div className="decor-right">
              <span></span>
            </div>
          </div>
        </div>
      </section>

      <div className="category-buttons">
        <button
          className={activeCategory === 'all' ? 'active' : ''}
          onClick={() => handleCategoryClick('all')}
          style={{ paddingInline: '3%' }}
        >
          All
        </button>
        <button
          className={activeCategory === 'cuisines' ? 'active' : ''}
          onClick={() => handleCategoryClick('cuisines')}
        >
          Cuisines
        </button>
        <button
          className={activeCategory === 'bake' ? 'active' : ''}
          onClick={() => handleCategoryClick('bake')}
        >
          Baked Items
        </button>
        <button
          className={activeCategory === 'vegetables' ? 'active' : ''}
          onClick={() => handleCategoryClick('vegetables')}
        >
          Vegetables
        </button>
        <button
          className={activeCategory === 'desserts' ? 'active' : ''}
          onClick={() => handleCategoryClick('desserts')}
        >
          Desserts
        </button>
        <button
          className={activeCategory === 'beverages' ? 'active' : ''}
          onClick={() => handleCategoryClick('beverages')}
        >
          Beverages
        </button>
      </div>


      <div className="category-content" style={{ marginBottom: '150px' }}>

                {activeCategory === 'all' && recipes.length > 0 ? (
                  filterRecipes.map((iteminfo) => (
                    <div className="recipe-card" key={iteminfo.items_id}>
                      <Link to={`/detail/${iteminfo.items_id}`} style={{ textDecoration: 'none'}} >
                        <div
                          className="recipe-card-content"
                          style={{
                            padding: "10px 30px 0px 30px",
                            borderRadius: "10px",
                            width: "300px",
                            height: "300px",
                          }}>
                          <div className="recipe-image">
                            <img
                              src={`http://localhost:8000/uploads/${encodeURIComponent(iteminfo.item_image_url)}`}
                              alt="Recipe Image"
                              style={{ width: '300px', height: '200px',borderRadius:'10px'}}
                            />
                          </div>
                          <div className="recipe-details">
                            <h3>Learn to make</h3>
                            <h2 className="recipe-name">{iteminfo.recipe_name}</h2>
                         
                          </div>
                        </div>
                      </Link>
                    </div>
                  ))
                ) : activeCategory === 'all' ? (
                  <p style={{ margin: 'auto' }}>No recipes available.</p>
                ) : null}


        {activeCategory === 'cuisines' && getRecipesByCategory('Cuisines').length > 0 ? (
                  getRecipesByCategory('Cuisines').map((iteminfo) => (
                    <div className="recipe-card" key={iteminfo.items_id}>
                      <Link to={`/detail/${iteminfo.items_id}`} style={{ textDecoration: 'none'}} >
                      <div
                          className="recipe-card-content"
                          style={{
                            padding: "10px 30px 0px 30px",
                            borderRadius: "4px",
                            width: "300px",
                            height: "300px",
                            marginRight:'10px',
                            textDecoration:'none',
                          }}>
                          <div className="recipe-image">
                            <img
                              src={`http://localhost:8000/uploads/${encodeURIComponent(iteminfo.item_image_url)}`}
                              alt="Recipe Image"
                              style={{ width: '300px', height: '200px' }}
                            />
                          </div>
                          <div className="recipe-details">
                          <h3>Learn to make</h3>
                          <h2 className="recipe-name" style={{textDecoration:'none'}}>{iteminfo.recipe_name}</h2>
                       
                          </div>
                        </div>
                      </Link>
                    </div>
                  ))
                ) : activeCategory === 'Cuisines' ? (
                  <p style={{ margin: 'auto' }}>No recipes available in the Cuisines category.</p>
                ) : null}



                {activeCategory === 'bake' && getRecipesByCategory('Bake').length > 0 ? (
                  getRecipesByCategory('Bake').map((iteminfo) => (
                    <div className="recipe-card" key={iteminfo.items_id}>
                      <Link to={`/detail/${iteminfo.items_id}`} style={{ textDecoration: 'none'}} >
                      <div
                          className="recipe-card-content"
                          style={{
                            padding: "10px 30px 0px 30px",
                            borderRadius: "4px",
                            width: "300px",
                            height: "300px",
                            marginRight:'10px',
                          }}>
                          <div className="recipe-image">
                            <img
                              src={`http://localhost:8000/uploads/${encodeURIComponent(iteminfo.item_image_url)}`}
                              alt="Recipe Image"
                              style={{ width: '300px', height: '200px' }}
                            />
                          </div>
                          <div className="recipe-details">
                          <h3>Learn to make</h3>
                            <h2 className="recipe-name">{iteminfo.recipe_name}</h2>
                            
                          </div>
                        </div>
                      </Link>
                    </div>
                  ))
                ) : activeCategory === 'bake' ? (
                  <p style={{ margin: 'auto' }}>No recipes available in the Baked Item category.</p>
                ) : null}


                {activeCategory === 'vegetables' && getRecipesByCategory('Vegetables').length > 0 ? (
                  getRecipesByCategory('Vegetables').map((iteminfo) => (
                    <div className="recipe-card" key={iteminfo.items_id} >
                      <Link to={`/detail/${iteminfo.items_id}`} style={{ textDecoration: 'none'}} >
                      <div
                          className="recipe-card-content"
                          style={{
                            padding: "10px 30px 0px 30px",
                            borderRadius: "4px",
                            width: "300px",
                            height: "300px",
                            marginRight:'10px',
                          }}>
                          <div className="recipe-image">
                            <img
                              src={`http://localhost:8000/uploads/${encodeURIComponent(iteminfo.item_image_url)}`}
                              alt="Recipe Image"
                              style={{ width: '300px', height: '200px' }}
                            />
                          </div>
                          <div className="recipe-details">
                          <h3>Learn to make</h3>
                            <p className="recipe-name" style={{textDecoration:'none'}}>{iteminfo.recipe_name}</p>
                        
                          </div>
                        </div>
                      </Link>
                    </div>
                  ))
                ) : activeCategory === 'vegetables' ? (
                  <p style={{ margin: 'auto' }}>No recipes available in the Vegetables category.</p>
                ) : null}



                {activeCategory === 'desserts' && getRecipesByCategory('Desserts').length > 0 ? (
                  getRecipesByCategory('Desserts').map((iteminfo) => (
                    <div className="recipe-card" key={iteminfo.items_id}>
                      <Link to={`/detail/${iteminfo.id}`} style={{ textDecoration: 'none'}} >
                      <div
                          className="recipe-card-content"
                          style={{
                            padding: "10px 30px 0px 30px",
                            borderRadius: "4px",
                            width: "300px",
                            height: "300px",
                            marginRight:'10px',
                          }}>
                          <div className="recipe-image">
                            <img
                              src={`http://localhost:8000/uploads/${encodeURIComponent(iteminfo.item_image_url)}`}
                              alt="Recipe Image"
                              style={{ width: '300px', height: '200px' }}
                            />
                          </div>
                          <div className="recipe-details">
                          <h3>Learn to make</h3>
                            <h2 className="recipe-name" >{iteminfo.recipe_name}</h2>
                            
                          </div>
                        </div>
                      </Link>
                    </div>
                  ))
                ) : activeCategory === 'desserts' ? (
                  <p style={{ margin: 'auto',fontSize:'18px', }}>No recipes available in the Desserts category.</p>
                ) : null}

       
                {activeCategory === 'beverages' && getRecipesByCategory('Beverages').length > 0 ? (
                  getRecipesByCategory('Beverages').map((iteminfo) => (
                    <div className="recipe-card" key={iteminfo.items_id} style={{ width: '200px', marginRight: '100px' }}>
                      <Link to={`/detail/${iteminfo.items_id}`} style={{ textDecoration: 'none'}} >
                      <div
                          className="recipe-card-content"
                          style={{
                            padding: "10px 30px 0px 30px",
                            borderRadius: "4px",
                            width: "300px",
                            height: "300px",
                            marginRight:'10px',
                          }}>
                          <div className="recipe-image">
                            <img
                              src={`http://localhost:8000/uploads/${encodeURIComponent(iteminfo.item_image_url)}`}
                              alt="Recipe Image"
                              style={{ width: '300px', height: '200px' }}
                            />
                          </div>
                          <div className="recipe-details">
                           <h3>Learn to make</h3>
                            <p className="recipe-name" style={{textDecoration:'none'}}>{iteminfo.recipe_name}</p>
                        
                          </div>
                        </div>
                      </Link>
                    </div>
                  ))
                ) : activeCategory === 'beverages' ? (
                  <p style={{ margin: 'auto' }}>No recipes available in the Vegetables category.</p>
                ) : null}
      </div>

      <Footer />
    </>
  );
};

export default HomePage;