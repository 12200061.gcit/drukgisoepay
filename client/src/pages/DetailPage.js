// import React, { useState, useEffect } from 'react';
// import axios from 'axios';
// import { ToastContainer, toast } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';
// import AccessTimeIcon from '@mui/icons-material/AccessTime';
// import RestaurantMenuIcon from '@mui/icons-material/RestaurantMenu';
// import WhatshotIcon from '@mui/icons-material/Whatshot';
// import Navbar from '../components/Header/Navbar';
// import Footer from '../components/Footer/Footer';
// import bakery from '../assets/images/bakery.jpeg'
// function DetailPage() {
//   const [item, setItems] = useState([]);

//   useEffect(() => {
//     fetchItems();
//   }, []);
//   const fetchItems = async () => {
//     try {
//       const response = await axios.get('http://localhost:8000/recipe');
//       setItems(response.data);
//     } catch (error) {
//       console.error('Error fetching items:', error);
//       toast.error('Failed to fetch items');
//     }
//   };
  
//   return (
//     <>
//        <Navbar/>
//        <section className="about-address-area">
//        <div className='container'>

//        <div style={{width:'100%',display:'flex',justifyContent:'space-between'}}>
//         <div>
//         <img src={bakery} style={{width:'400px',height:'400px'}}></img>
//         <p style={{fontSize:"24px",fontWeight:'bold',marginTop:'20px'}}>{item.recipe_name}</p>
//         <p style={{fontSize:"16px",fontWeight:'normal',marginBottom:'20px'}}>{item.category}</p>

//         <div style={{ display: 'flex', justifyContent: 'space-between' }}>
//       <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
//         <AccessTimeIcon style={{height:"20px",width:'20px'}}/>
//         <div style={{fontSize:'16px',fontWeight:'bold'}}>Prep time</div>
//         <div>30 min</div>

//       </div>
//       <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
//         <RestaurantMenuIcon style={{height:"20px",width:'20px'}}/>
//         <div style={{fontSize:'16px',fontWeight:'bold'}}>Cooking time</div>
//         <div>30 min</div>

//       </div>
//       <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
//       <WhatshotIcon style={{height:"20px",width:'20px'}}/>
//         <div style={{fontSize:'16px',fontWeight:'bold'}}>Calories</div>
//         <div>450 cal</div>

//       </div>
//     </div>




//         </div>
//         <span  style={{width:'700px'}}>
//             <p style={{fontSize:"24px",fontWeight:'bold',marginBottom:'20px'}}>Description</p>
//             <p>Our mission is to empower individuals to explore the world of culinary delights,
//                 connect with their passion for cooking, and create memorable dining experiences. 
//                 We strive to be the go-to platform for food enthusiasts of all levels, providing 
//                 a comprehensive collection of diverse and delectable recipes from around the globe
//             </p>

//             <p style={{fontSize:"24px",fontWeight:'bold',marginTop:'20px',marginBottom:'20px'}}>Our Vision</p>
//             <p>Our vision is to revolutionize the way people approach cooking and eating by becoming
//                the ultimate companion for food enthusiasts worldwide. We envision a future where 
//                individuals embrace the joy of cooking, explore diverse flavors, and savor extraordinary culinary experiences.
//             </p>
//             <p style={{fontSize:"24px",fontWeight:'bold',marginTop:'20px',marginBottom:'20px'}}>Directions</p>
//             <p>Our vision is to revolutionize the way people approach cooking and eating by becoming
//                the ultimate companion for food enthusiasts worldwide. We envision a future where 
//                individuals embrace the joy of cooking, explore diverse flavors, and savor extraordinary culinary experiences.
//             </p>
//         </span>
//      </div>
//      </div>
//      </section>
//        <Footer/>
//     </>
 
//   )
// }

// export default DetailPage
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import RestaurantMenuIcon from '@mui/icons-material/RestaurantMenu';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import Navbar from '../components/Header/Navbar';
import Footer from '../components/Footer/Footer';
import bakery from '../assets/images/bakery.jpeg';

function DetailPage() {
  const { id } = useParams();
  const [items, setItem] = useState([]);

  useEffect(() => {
    fetchItem();
  }, []);

  const fetchItem = async () => {
    try {
      const response = await axios.get(`http://localhost:8000/detail/${id}`);
      setItem(response.data);
    } catch (error) {
      console.error('Error fetching item:', error);
      toast.error('Failed to fetch item');
    }
  };


  return (
    <>
      <Navbar />
      <section className="about-address-area">
        <div className="container">
          {items.map((item)=>(
                      <div style={{ width: '100%', display: 'flex', justifyContent: 'space-between' }}>
            <div>
              <img src={`http://localhost:8000/uploads/${encodeURIComponent(item.item_image_url)}`} style={{ width: '500px', height: '400px',marginBottom:'50px',borderRadius:'10px'
 }} alt="no img" />
              <p style={{ fontSize: '24px', fontWeight: 'bold', marginTop: '20px' }}>{item.recipe_name}</p>
              <p style={{ fontSize: '18px', fontWeight: 'normal', marginBottom: '20px', }}><b>category:</b> {item.category}</p>

              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                  <AccessTimeIcon style={{ height: '20px', width: '20px' }} />
                  <div style={{ fontSize: '18px', fontWeight: 'bold' }}>Prep time</div>
                  <div style={{fontSize:'16px'}}>{item.preptime}</div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                  <RestaurantMenuIcon style={{ height: '20px', width: '20px' }} />
                  <div style={{ fontSize: '18px', fontWeight: 'bold' }}>Cooking time</div>
                  <div  style={{fontSize:'16px'}}>{item.cookingtime}</div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                  <WhatshotIcon style={{ height: '20px', width: '20px' }} />
                  <div style={{ fontSize: '18px', fontWeight: 'bold' }}>Calories</div>
                  <div  style={{fontSize:'16px'}}>{item.calories}</div>
                </div>
              </div>
            </div>
            <span style={{ width: '700px' }}>
              <div>
              <p style={{ fontSize: '24px', fontWeight: 'bold', marginBottom: '20px' }}>Description</p>
              <p style={{fontSize:"18px",fontWeight:'500'}}>{item.description}</p>
              </div>
              <div>
              <p style={{ fontSize: '24px', fontWeight: 'bold', marginBottom: '20px' }}>Ingredients</p>
              <p style={{fontSize:"18px",fontWeight:'500'}}>{item.ingredients}</p>
              <div>
              <p style={{ fontSize: '24px', fontWeight: 'bold', marginBottom: '20px' }}>Steps in making {item.recipe_name}</p>
              <p style={{fontSize:"18px",fontWeight:'500'}}>{item.steps}</p>
              </div>
              </div>
             

              {/* Render other details */}
            </span>
          </div>
            
          ))}

        </div>
      </section>
      <Footer />
    </>
  );
}

export default DetailPage;
