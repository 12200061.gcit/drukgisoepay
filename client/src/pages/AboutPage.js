import React from 'react'
import "./AboutPage.css";
import Navbar from '../components/Header/Navbar';
import Footer from '../components/Footer/Footer';
import me from '../assets/images/me.jpeg';
import bakery from '../assets/images/bakery.jpeg';


function AboutPage() {
  const developers = [
    {
      id: 1,
      name: 'Gyem Tshering',
      role: 'Full stack Developer',
      image: bakery

    },
    {
      id: 2,
      name: 'Kunzang Dolkar',
      role: 'UI/UX Developer',
      image: bakery
    },
    {
      id: 3,
      name: 'Cholen',
      role: 'Frontend developer',
      image: bakery

    },
    {
      id: 4,
      name: 'Kuenley Tshewang',
      role: 'Backend Designer',
      image: bakery

    },
 
  ];
  
  return (
    <>
    <Navbar />

     <div className='about'>
    <div className='about-content flex align-center justify-center flex-column text-center'>
      <p className='text-white about-title ls-2' style={{fontSize:'45px',fontWeight:"bold",}}>About Us</p>
      <p className='text text-white my-3 ls-1' style={{fontSize:'16px'}}><br></br>Passionate experts dedicated to delivering innovative solutions and exceptional customer experiences</p>    </div>
  </div>
  <section className="about-address-area">
  <div className='container'>
  <div className="sec-title-style1 text-center max-width">
       <div className="text" >
         <div className="decor-left">
           <span></span>
         </div>
         <p style={{fontSize:'24px'}}>Learn More About Us</p>
         <div className="decor-right">
           <span></span>
         </div>
       </div>
       <div className="bottom-text" s>
         <p>
         
         </p>
       </div>


     </div>
     <div style={{width:'100%',display:'flex',justifyContent:'space-between'}}>
        <img src={bakery} style={{width:'400px',height:'400px',border:'5px solid #4cbb17'}}></img>
        <span  style={{width:'700px'}}>
          <p style={{fontSize:"36px",fontWeight:'bold',marginBottom:'20px'}}>Our Mission</p>
            <p style={{fontSize:'16px'}}>Our mission is to empower individuals to explore the world of culinary delights,
             connect with their passion for cooking, and create memorable dining experiences. 
             We strive to be the go-to platform for food enthusiasts of all levels, providing 
             a comprehensive collection of diverse and delectable recipes from around the globe</p>
            <p style={{fontSize:"36px",fontWeight:'bold',marginTop:'20px',marginBottom:'20px'}}>Our Vision</p>
            <p style={{fontSize:'16px'}}>Our vision is to revolutionize the way people approach cooking and eating by becoming
               the ultimate companion for food enthusiasts worldwide. We envision a future where 
               individuals embrace the joy of cooking, explore diverse flavors, and savor extraordinary culinary experiences.</p>
        </span>
     </div>


     <div>
      <div style={{ display: 'flex', justifyContent: 'center', marginTop:'100px' }}>
        {developers.map((developer) => (
          <div
            key={developer.id}
            style={{
              margin: '20px',
              padding: '20px',
              borderRadius: '4px',
              textAlign: 'center',
              width: '300px'
            }}
          >
            <img
              src={developer.image}
              alt="Developer"
              style={{
                width: '200px',
                height: '200px',
                borderRadius: '50%',
                objectFit: 'cover',
                marginBottom: '30px',
                marginLeft:'20px'
              }}
            />
            <p style={{fontSize:'20px',fontWeight:'bold'}}>{developer.name}</p>
            <p style={{fontSize:'18px'}}>{developer.role}</p>
            
          </div>
          
        ))}
        
      </div>
    </div>
  </div>
  </section>
  <Footer/>
    </>
    )
}

export default AboutPage