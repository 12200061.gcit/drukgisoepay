import "./ContactPage.css";
import Navbar from '../components/Header/Navbar';
import Footer from '../components/Footer/Footer';
import React, { useState } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import img1 from '../assets/images/img1.jpeg'
const ContactPage = () => {
  const [uploadStatus, setUploadStatus] = useState('');
    const [canUpload, setCanUpload] = useState(false);   
    const [username,setUsername] =useState('');
    const [email,setEmail] =useState('');
    const [message,setMessage] =useState('');
  
    const handleUpload = async () => {
      try {
        setUploadStatus('Uploading...');
    
        const formData = new FormData();
        formData.append('username', username);
        formData.append('message', message);
        formData.append('email', email);
        console.log(username)
        console.log(email)
        console.log(message)
        const response = await axios.post('https://drukgisoepay.onrender.com/upload',{username,message,email
        });
    
        setUploadStatus('Upload successful!');
        window.location.reload();
      } catch (error) {
        if (error.response && error.response.data) {
          toast.error(error.response.data.message);
        } else {
          toast.error('An error occurred. Please try again.');
        }
    
        console.error(error);
        setUploadStatus('Upload failed. Please try again.');
      }
    };
  return (
    <>
    <Navbar />

    <div className='contact'>
        <div className='contact-content flex align-center justify-center flex-column text-center'>
        <p className='text about-title ls-2' style={{fontSize:'45px',fontWeight:"bold",color:'#fff'}}>Contact Us</p>
        <p className='text text-white my-3 ls-1' style={{fontSize:'16px'}}><br></br>Questions? Reach out to us for prompt assistance, we're here to help.
</p>        </div>
      </div>  
      <section className="contact-address-area">
        <div>
          <div className="sec-title-style1 text-center max-width">
            <div className="text">
              <div className="decor-left">
                <span></span>
              </div>
              <p style={{fontSize:'24px'}}>Reach out for Us</p>
              <div className="decor-right">
                <span></span>
              </div>
            </div>
          
          </div>
          </div>
      </section>
    
      <div className="container" >
      <div className="content">
        <div className="left-side">
          <div className="address details">
            <i className="fas fa-map-marker-alt"></i>
            <div className="topic"></div>
            <div className="text-one"></div>
            <div className="text-two"></div>
          </div>
          <div className="phone details">
            <i className="fas fa-phone-alt"></i>
            <div className="topic"></div>
            <div className="text-one"></div>
            <div className="text-two"></div>
          </div>
          <div className="email details">
            <i className="fas fa-envelope"></i>
            <div className="topic"></div>
            <div className="text-one"></div>
            <div className="text-two"></div>
          </div>
        </div>
        <div className="right-side">
          <div className="topic-text" style={{fontSize:'18px',fontWeight:'bold'}}>Send us a message</div>
          <form action="#">
            <div className="input-box">
              <input type="text" placeholder="Enter your name" value={username} onChange={(e)=>{setUsername(e.target.value)}} />
            </div>
            <div className="input-box">
              <input type="text" placeholder="Enter your email" value={email} onChange={(e)=>{setEmail(e.target.value)}}/>
            </div>
            <div className="input-box message-box">
              <textarea placeholder="Enter your message" value={message} onChange={(e)=>{setMessage(e.target.value)}}></textarea>
            </div>
            <div className="button">
              <input type="button" value="Send Now" onClick={handleUpload}/>
            </div>
          </form>
        </div>
      </div>
      <div class="info-row">
        <div class="info-box1" >
          <p>Our Office</p>
          <h3>Thimphu,Kabesa</h3>

        </div>
        <div class="info-box2">
          <p>Phone Number</p>
          <h3>+975 7739 8819</h3>

        </div>
        <div class="info-box3">
          <p>Email</p>
          <h3>drukgisoepay@gmail.com</h3>

        </div>
        <div class="info-box4">
          <p>Fax</p>
          <h3>1-232-454-23</h3>
        </div>
</div>

    </div>
 
    <Footer/>
    </>
  )
}

export default ContactPage