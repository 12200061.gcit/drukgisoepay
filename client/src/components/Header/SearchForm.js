import React from 'react';
import "./Navbar";
import { BsSearch } from "react-icons/bs";

const SearchForm = ({handleSearch}) => {

  return (
    <form className='search-form flex align-center'>
      <input type = "text" className='form-control-input text-dark-gray fs-15' placeholder='Search recipes here ...' onChange={handleSearch} />
     
    </form>
  )
}

export default SearchForm