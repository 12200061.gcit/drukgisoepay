import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/logo.png';
import axios from "axios";
import { useNavigate } from 'react-router-dom';

const Navbar = () => {
  const Navigate = useNavigate();
  const handleLogout = async () => {
    try {
      const response = await axios.post("http://localhost:8000/logout", {
      });
      console.log(response.data); 
      
      localStorage.removeItem("authToken");
      localStorage.removeItem("isAdmin");
      localStorage.removeItem("userid");
     
      Navigate("/");
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <nav style={{ backgroundColor: '#4cbb17',padding:"10px 0px" }}>
      <div>
        <div className='navbar-content text-white' style={{ paddingLeft: '150px', paddingRight: '150px', }}>
          <div className='brand-and-toggler flex align-center justify-between' style={{height:'80px',}}>
            <Link to='/home' className='navbar-brand fw-3 fs-22 flex align-center'>
              <img src={logo} style={{ height: 60, width: 250 }} alt="Logo" />
            </Link>
            <div>
              <ul style={{ display: 'flex', justifyContent: 'space-between' }}>
                <li>
                  <Link to='/home' style={{ marginRight: 30, color: 'white',fontSize:'20px',textDecoration:'none',fontWeight:'500' }}>Home</Link>
                </li>
                <li>
                  <Link to='/about' style={{ marginRight: 30, color: 'white',fontSize:'20px',textDecoration:'none' ,fontWeight:'500' }}>About Us</Link>
                </li>
                <li>
                  <Link to='/contact' style={{ marginRight: 30, color: 'white',fontSize:'20px',textDecoration:'none',fontWeight:'500' }}>Contact Us</Link>
                </li>
                <li style={{ marginRight: 20,fontSize:'20px',textDecoration:'none' }}><button onClick={handleLogout} style={{color:'white',fontWeight:'bold'}}>Logout</button></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;