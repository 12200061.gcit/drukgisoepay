import {FaFacebook, FaTwitter, FaLinkedin, FaGithub } from 'react-icons/fa';
import './Footer.css'
export default function Footer() {
  return (
    <div>
      <footer className="footer-distributed">
        <div className="footer-left">
        <p style={{fontWeight:"700",fontSize:'24px',marginTop:'20px',color:'#fff'}}>Drukgi Soepay</p>
          <h4 className="footer-links">
            <a href="#" className="link-1">Why don't you try your own Recipe</a>
          </h4>
        </div>
        <div className="footer-center">
        <p className="footer-company">
            <span style={{fontSize:"20px"}}>Know More About Us</span>
            <br></br>
            <div style={{marginBottom:'10px',fontSize:'16px', fontWeight:'lighter'}}>Email info: drukgisoepay@gmail.com</div>
            <div style={{marginBottom:'10px',fontSize:'16px', fontWeight:'lighter'}}>Location: Thimphu,Kabesa</div>
            <div style={{marginBottom:'10px',fontSize:'16px', fontWeight:'lighter'}}>Contact No: +975 77345232</div>
          </p>
         
        </div>
        <div className="footer-right">
          <p className="footer-company-about">
            <span style={{fontSize:'20px'}}>About the company</span>
            <p style={{fontSize:'16px'}}>Discover new recipes, organize your ingredients, and cook with ease. Tashi Delek!</p>
          </p>
          <div className="footer-icons">
            <a href="#"><FaFacebook /></a>
            <a href="#"><FaTwitter /></a>
            <a href="#"><FaLinkedin /></a>
            <a href="#"><FaGithub /></a>
          </div>
        </div>
      </footer>
    </div>
  );
}
