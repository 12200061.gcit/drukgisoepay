import './App.css'
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

import AboutPage from './pages/AboutPage';
import ContactPage from './pages/ContactPage';
import DetailPage from './pages/DetailPage'
import SignUp from './pages/SignUp';
import SignIn from './pages/SignIn';
import HomePage from './pages/HomePage';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
          <BrowserRouter>
              <Routes>
                <Route path="/" element={<SignIn/>} />
                <Route path="/signup" element={<SignUp />} />
                <Route path="/Home" element={<HomePage/>} />
                <Route path="/about" element={<AboutPage />} />
                <Route path="/contact" element={<ContactPage />} />
                <Route path="/detail/:id" element={<DetailPage />} />

              </Routes>  
          </BrowserRouter>
  );
}

export default App;