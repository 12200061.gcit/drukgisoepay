const express = require('express');
const router = express.Router();
const recipeController = require('../controllers/recipeController');
const authController = require('../controllers/authController');

// Define a route for creating a recipe
router.post('/createRecipe', recipeController.createRecipe);

// Routes for recipes
router.route('/')
  .get(recipeController.getAllRecipes);

router.route('/:id')
  .get(recipeController.getRecipe)
  .patch(recipeController.updateRecipe)
  .delete(recipeController.deleteRecipe);

module.exports = router;
